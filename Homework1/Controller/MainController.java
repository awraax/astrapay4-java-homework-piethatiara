package Homework1.Controller;

import Homework1.Model.Manager;
import Homework1.Model.Staff;

import java.io.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class MainController {
    Scanner input = new Scanner(System.in);
    int idWorker, jabatan;
    String namaWorker, nomorWorker, emailWorker;
    long tunjangMakan, tunjangPulsa, gajiWorker, tunjangTransport, tunjangEntertain;

    ArrayList<Staff> arrayStaff = new ArrayList<>();
    ArrayList<Manager> arrayManager = new ArrayList<>();

    public void menuWorker(){
        System.out.println("Karyawan Perusahaan");
        System.out.println("1. Staff Perusahaan");
        System.out.println("2. Manager Perusahaan");
        System.out.print("Pilih = ");
        jabatan = input.nextInt();

        switch (jabatan){
            case 1:
                System.out.print("ID Karyawan : ");
                idWorker = input.nextInt();
                System.out.print("Nama Karyawan : ");
                namaWorker = input.next();
                System.out.print("Tunjangan Pulsa Karyawan : ");
                tunjangPulsa = input.nextLong();
                System.out.print("Gaji Karyawan : ");
                gajiWorker = input.nextLong();
                System.out.print("Tunjangan Makan Karyawan : ");
                tunjangMakan = input.nextLong();

                ArrayList<String> emailKar = new ArrayList<String>();
                String pilihanKar = "y";
                while (Objects.equals(pilihanKar, "y")){
                    System.out.print("Email Karyawan : ");
                    emailWorker= input.next();
                    emailKar.add(emailWorker);
                    System.out.println("Masukkan email karyawan lagi? (y/n)");
                    pilihanKar = input.next();
                }

                Staff staff = new Staff(idWorker, namaWorker, tunjangPulsa, gajiWorker, tunjangMakan, emailKar);
                arrayStaff.add(staff);
                break;
            case 2:
                System.out.print("ID Manajer : ");
                idWorker = input.nextInt();
                System.out.print("Nama Manajer : ");
                namaWorker = input.next();
                System.out.print("Tunjangan Pulsa Manajer : ");
                tunjangPulsa = input.nextLong();
                System.out.print("Gaji Manajer : ");
                gajiWorker = input.nextLong();
                System.out.print("Tunjangan Transport Manajer : ");
                tunjangTransport = input.nextLong();
                System.out.print("Tunjangan Entertain Manajer : ");
                tunjangEntertain = input.nextLong();

                ArrayList<String> nomorMan = new ArrayList<String>();
                String pilihanMan = "y";
                while (Objects.equals(pilihanMan, "y")){
                    System.out.print("Nomor Manajer : ");
                    nomorWorker= input.next();
                    nomorMan.add(emailWorker);
                    System.out.println("Masukkan nomor manajer lagi? (y/n)");
                    pilihanMan = input.next();
                }
                Manager manajer = new Manager(idWorker, namaWorker, tunjangPulsa, gajiWorker, tunjangTransport, tunjangEntertain, nomorMan);
                arrayManager.add(manajer);
                break;

        }
    }

    public void menuCreateJson(){

        //Staff
        try{
            FileWriter fileWriter = new FileWriter("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Homework/src/Homework1/Output/Staff_Homework1.txt");
            JSONArray arStaff = new JSONArray();

            for (Staff staff:arrayStaff){
                JSONObject objStaff = new JSONObject();
                objStaff.put("id", staff.getIdKar());
                objStaff.put("nama", staff.getNamaKar());
                objStaff.put("tunjangan pulsa", staff.getTunjanganPulsa());
                objStaff.put("gaji", staff.getGajiKar());
                objStaff.put("absensi", staff.getAbsenKar());
                objStaff.put("tunjangan makan", staff.getTunjanganMakan());

                JSONArray arEmail = new JSONArray();
                for (String mail : staff.getEmailKar()){
                    arEmail.add(mail);
                }
                objStaff.put("email", arEmail);
                arStaff.add(objStaff);
            }
            fileWriter.write(String.valueOf(arStaff));
            fileWriter.close();
        }catch (Exception e){
            System.out.println(e);
        }
        System.out.println("Success");

        try {
            FileWriter fileWriter = new FileWriter("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Homework/src/Homework1/Output/Manager_Homework1.txt");
            JSONArray arManager = new JSONArray();

            for (Manager m : arrayManager){
                JSONObject objManager = new JSONObject();
                objManager.put("id", m.getIdKar());
                objManager.put("nama", m.getNamaKar());
                objManager.put("tunjangan pulsa", m.getTunjanganPulsa());
                objManager.put("gaji", m.getGajiKar());
                objManager.put("absens", m.getAbsenKar());
                objManager.put("tunjangan transport", m.getTunjanganTransport());
                objManager.put("tunjangan entertainment", m.getTunjanganEntertain());

                JSONArray arNomor = new JSONArray();
                for (String nomor : m.getNomorManager()){
                    arNomor.add(nomor);
                }
                objManager.put("telepon", arNomor);
                arManager.add(objManager);
            }
            fileWriter.write(String.valueOf(arManager));
            fileWriter.close();
        }catch (Exception e){
            System.out.println(e);
        }
        System.out.println("Success");
    }

    public void menuReadJson() throws Exception{
        System.out.println("Masukkan nama file : ");
        String namaFile = input.next();
        System.out.println();

        FileReader fileReader = new FileReader("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Homework/src/Homework1/Output/"+namaFile);
        String strResult = "";

        //Read Character
        int i;
            while ((i = fileReader.read())!=-1){
                strResult =strResult + (char) i;
            }

            JSONParser parser = new JSONParser();
            Reader reader = new StringReader(strResult);
            Object jsonObj = parser.parse(reader);

            ArrayList<JSONObject>  jsStaff = (ArrayList<JSONObject>) jsonObj;
            for (JSONObject jsonObject: jsStaff){
                System.out.println("ID Karyawan = " + jsonObject.get("id"));
                System.out.println("Nama Karyawan = " + jsonObject.get("nama"));
                System.out.println("Tunjangan Pulsa Karyawan = " +jsonObject.get("tunjangan pulsa"));
                System.out.println("Gaji Karyawan = " +jsonObject.get("gaji"));
                System.out.println("Absensi = "+jsonObject.get("absensi"));

                if (namaFile.equals("Staff_Homework1.txt")){
                    System.out.println("Tunjangan Makan Karyawan " + jsonObject.get("tunjangan makan"));

                    ArrayList<String> emailStaff = (ArrayList<String>) jsonObject.get("email");
                    System.out.println("Email = ");
                    for (String email : emailStaff){
                        System.out.println(email);
                    }
                    System.out.println();
                } else if (namaFile.equals("Manager_Homework1.txt")) {
                    System.out.println("Tunjangan Transport = "+jsonObject.get("tunjangan transport"));
                    System.out.println("Tunjangan Entertainment = "+jsonObject.get("tunjangan entertainment"));

                    ArrayList<String> teleponMana = (ArrayList<String>) jsonObject.get("telepon");
                    System.out.println("Nomor Telepon Manager = ");
                    for (String nomor : teleponMana){
                        System.out.println(nomor);
                    }

                }
            }
            System.out.println();
            fileReader.close();
    }
}
