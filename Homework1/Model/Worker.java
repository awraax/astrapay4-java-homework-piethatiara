package Homework1.Model;

public abstract class Worker {
    int idKar;
    String namaKar;
    long tunjanganPulsa;
    long gajiKar;
    int absenKar = 20;

    public Worker(int idKar, String namaKar, long tunjanganPulsa,long gajiKar){
        this.idKar = idKar;
        this.namaKar = namaKar;
        this.tunjanganPulsa = tunjanganPulsa;
        this.gajiKar = gajiKar;
    }

    public int getIdKar() {
        return idKar;
    }

    public String getNamaKar() {
        return namaKar;
    }

    public long getTunjanganPulsa() {
        return tunjanganPulsa;
    }

    public long getGajiKar() {
        return gajiKar;
    }

    public int getAbsenKar() {
        return absenKar;
    }
}
