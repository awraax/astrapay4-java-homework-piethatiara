package Homework1.Model;

import java.util.ArrayList;

public class Staff extends Worker{
    long tunjanganMakan;
    ArrayList<String> emailKar = new ArrayList<>();

    public Staff(int idKar, String namaKar, long tunjanganPulsa, long gajiKar, long tunjanganMakan, ArrayList<String> emailKar) {
        super(idKar, namaKar, tunjanganPulsa, gajiKar);
        this.tunjanganMakan = tunjanganMakan;
        this.emailKar = emailKar;

    }

    public long getTunjanganMakan() {
        return tunjanganMakan;
    }

    public ArrayList<String> getEmailKar() {
        return emailKar;
    }
}
