package Homework1.Model;

import java.util.ArrayList;

public class Manager extends Worker{
    long tunjanganTransport, tunjanganEntertain;
    ArrayList<String> nomorManager = new ArrayList<>();


    public Manager(int idKar, String namaKar, long tunjanganPulsa, long gajiKar, long tunjanganTransport, long tunjanganEntertain, ArrayList<String> nomorManager) {
        super(idKar, namaKar, tunjanganPulsa, gajiKar);
        this.tunjanganTransport = tunjanganTransport;
        this.tunjanganEntertain = tunjanganEntertain;
        this.nomorManager = nomorManager;
    }

    public long getTunjanganTransport() {
        return tunjanganTransport;
    }

    public long getTunjanganEntertain() {
        return tunjanganEntertain;
    }

    public ArrayList<String> getNomorManager() {
        return nomorManager;
    }
}
