package Homework1.View;

import Homework1.Controller.MainController;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        int pilih = 0;

        MainController mainController = new MainController();

        while (pilih != 99){
            System.out.println("1. Input Data Staff dan Manager");
            System.out.println("2. Create JSON");
            System.out.println("3. Read JSON");
            System.out.println("99. Exit");
            System.out.print("Silahkan Masukkan Pilihan : ");
            pilih = input.nextInt();

            switch (pilih){
                case 1:
                    mainController.menuWorker();
                    break;
                case 2:
                    mainController.menuCreateJson();
                    break;
                case 3:
                    mainController.menuReadJson();
                    break;
                case 99:
                    break;
                default:
                    System.out.println("Pilihan tidak tersedia");
            }
        }
    }
}
