package Homework2.View;

import Homework2.Controller.ControllerServer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

public class MainServer {
    public static void main(String[] args) {
        String port ="";

        try {
            try (InputStream fileCheck = new FileInputStream("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Homework/src/Homework2/Controller/config.properties")){
                Properties prop = new Properties();
                prop.load(fileCheck);
                port = prop.getProperty("port");

            }catch (IOException ex){
                ex.printStackTrace();
            }

            ServerSocket serverSocket = new ServerSocket(Integer.parseInt(port));
            Socket socket = serverSocket.accept();
            String serverPesan = "";
            ControllerServer controllerServer = new ControllerServer();

            while (!serverPesan.equals("exit")){
                DataInputStream din = new DataInputStream(socket.getInputStream());
                DataOutputStream dout = new DataOutputStream(socket.getOutputStream());

                serverPesan = (String) din.readUTF();
                System.out.println("Client choose menu "+serverPesan);

                if (serverPesan.equals(("request"))){
                    String serverOutput = controllerServer.requestMenu();
                    dout.writeUTF(serverOutput);
                    dout.flush();
                }
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
