package Homework2.View;

import Homework2.Controller.ControllerClient;

import java.io.*;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;

public class MainClient {
    static String clientPesan;
    static DataOutputStream dout;
    static Socket socket;

    public static void main(String[] args) throws EOFException{
        Scanner input = new Scanner(System.in);
        String ip = "", port = "",str = "";
        int pilih = 0;


        ControllerClient controllerClient = new ControllerClient();

        try {
            try (InputStream fileCheck = new FileInputStream("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Homework/src/Homework2/Controller/config.properties")){
                Properties prop = new Properties();
                prop.load(fileCheck);
                ip = prop.getProperty("ip");
                port = prop.getProperty("port");
            }catch (IOException ex){
                ex.printStackTrace();
            }
            socket = new Socket(ip, Integer.parseInt(port));
            dout = new DataOutputStream(socket.getOutputStream());
            DataInputStream din = new DataInputStream(socket.getInputStream());

            while (pilih != 99){
                System.out.println("MENU");
                System.out.println("1. Connect Socket");
                System.out.println("2. Create File");
                System.out.println("3. Make File Average");
                System.out.println("99. Exit");
                System.out.println("Pilih Menu : ");
                pilih = input.nextInt();

                if (pilih == 1) {
                    //send file
                    clientPesan = "Request";
                    dout.writeUTF(clientPesan);
                    dout.flush();

                    //receive file
                    str = (String) din.readUTF();
                    System.out.println(str);
                    System.out.println();

                } else if (pilih == 2) {
                        controllerClient.menu2(str);
                        System.out.println();
                } else if (pilih == 3) {
                        controllerClient.menu3();
                        System.out.println();

                } else if (pilih == 99) {
                    String clientPesan = "exit";
                    dout.writeUTF(clientPesan);
                    dout.flush();
                    break;
                }else{
                    System.out.println("Pilihan tidak ada");
                }
            }socket.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
