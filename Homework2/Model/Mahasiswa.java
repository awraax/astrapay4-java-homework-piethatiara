package Homework2.Model;

public class Mahasiswa {
    String nama;
    long fisika, kimia, biologi;

    public Mahasiswa(String nama, long fisika, long kimia, long biologi){
        this.nama = nama;
        this.fisika = fisika;
        this.kimia = kimia;
        this.biologi = biologi;
    }

    public String getNama() {
        return nama;
    }

    public long getFisika() {
        return fisika;
    }

    public long getKimia() {
        return kimia;
    }

    public long getBiologi() {
        return biologi;
    }
}
