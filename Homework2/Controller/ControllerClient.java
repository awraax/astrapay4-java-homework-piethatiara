package Homework2.Controller;

import Homework2.Model.Mahasiswa;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;


public class ControllerClient {

    ArrayList<Mahasiswa> arrayMahasiswa = new ArrayList<>();
    Mahasiswa mahasiswa;

    public void menu2(String konteks){
        try {
            JSONParser parser = new JSONParser();
            Reader reader = new StringReader(konteks);
            Object mahasiswaObject = parser.parse(reader);

            ArrayList<JSONObject> jsMahasiswa = (ArrayList<JSONObject>) mahasiswaObject;
            FileWriter fileWriter = new FileWriter("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Homework/src/Homework2/Output/FileProses.txt");
            for (JSONObject jsonMaha : jsMahasiswa){
                String namaMahasiswa = (String) jsonMaha.get("nama");
                long nilaiFisika = (Long) jsonMaha.get("nilaiFisika");
                long nilaiKimia = (Long) jsonMaha.get("nilaiKimia");
                long nilaiBiologi = (Long) jsonMaha.get("nilaiBiologi");

                fileWriter.write("Nama Mahasiswa = "+namaMahasiswa);
                fileWriter.write("Nilai Fisika = "+nilaiFisika);
                fileWriter.write("Nilai Kimia = "+nilaiKimia);
                fileWriter.write("Nilai Biologi = "+nilaiBiologi);
                mahasiswa = new Mahasiswa(namaMahasiswa, nilaiFisika, nilaiKimia, nilaiBiologi);
                arrayMahasiswa.add(mahasiswa);
            }fileWriter.close();
        }catch (Exception e){
            System.out.println(e);
        }
        System.out.println("Success");
    }

    public void menu3(){
        try {
            FileWriter fileWriter = new FileWriter("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Homework/src/Homework2/Output/FileRata.txt");
            JSONArray arrMaha = new JSONArray();

            for (Mahasiswa mahasiswa: arrayMahasiswa){
                JSONObject objMaha = new JSONObject();
                double rata = (mahasiswa.getFisika() + mahasiswa.getKimia() + mahasiswa.getBiologi())/3;
                objMaha.put("Nama Mahasiswa = ", mahasiswa.getNama());
                objMaha.put("Rata-Rata Nilai Mahasiswa = ", rata);
                arrMaha.add(objMaha);
            }
            fileWriter.write(String.valueOf(arrMaha));
            fileWriter.close();
        }catch (Exception e){
            System.out.println(e);
        }
        System.out.println("Success");
    }
}
